package test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.DecimalFormat;

import org.junit.jupiter.api.Test;

import JUnit.Junit09_Geometria.dto.Geometria;

class Test01 {
	Geometria testG =new Geometria();
	
	double redondear(double resultat) {
		resultat=(double) Math.round(resultat * Math.pow(10, 2))/ Math.pow(10, 2);
		return resultat;
		
	}

	@Test
	void testAreacuadrado() {
		int num1=4;
		assertEquals(testG.areacuadrado(num1),16);
	}

	@Test
	void testAreaCirculo() {
		int num1=3;
		Double resultat = redondear(testG.areaCirculo(num1));
		assertEquals(resultat,28.27);
		
	}

	@Test
	void testAreatriangulo() {
		int num1=5;
		int num2 = 10;
		assertEquals(testG.areatriangulo(num1,num2),25);
	}

	@Test
	void testArearectangulo() {
		int num1=4;
		int num2=5;
		assertEquals(testG.arearectangulo(num1,num2),20);
	}

	@Test
	void testAreapentagono() {
		int num1=5;
		int num2 = 10;
		assertEquals(testG.areapentagono(num1,num2),25);
	}

	@Test
	void testArearombo() {
		int num1=5;
		int num2 = 10;
		assertEquals(testG.arearombo(num1,num2),25);
	}

	@Test
	void testArearomboide() {
		int num1=4;
		int num2=5;
		assertEquals(testG.arearomboide(num1,num2),20);
	}

	@Test
	void testAreatrapecio() {
		int num1=4;
		int num2=5;
		int num3=8;
		assertEquals(testG.areatrapecio(num1,num2,num3),36);
	}

	@Test
	void testFigura() {
		assertEquals(testG.figura(1),"cuadrado");
		assertEquals(testG.figura(2),"Circulo");
		assertEquals(testG.figura(3),"Triangulo");
		assertEquals(testG.figura(4),"Rectangulo");
		assertEquals(testG.figura(5),"Pentagono");
		assertEquals(testG.figura(6),"Rombo");
		assertEquals(testG.figura(7),"Romboide");
		assertEquals(testG.figura(8),"Trapecio");
		assertEquals(testG.figura(9),"Default");
		assertEquals(testG.figura(0),"Default");
	}

	@Test
	void testGetId() {
		assertEquals(testG.getId(),9);
	}

	@Test
	void testSetId() {
		Geometria test2= new Geometria(2);
		test2.setId(4);
		assertEquals(test2.getId(),4);
	}

	@Test
	void testGetNom() {
		Geometria test3= new Geometria(2);
		assertEquals(test3.getNom(),"Circulo");
		
	}

	@Test
	void testSetNom() {
		Geometria test3= new Geometria(2);
		test3.setNom("Patata");
		assertEquals(test3.getNom(),"Patata");
	}

	@Test
	void testGetArea() {
		Geometria test3= new Geometria(2);
		assertEquals(test3.getArea(),3.0);
	}

	@Test
	void testSetArea() {
		Geometria test3= new Geometria(2);
		test3.setArea(3.0);
		assertEquals(test3.getArea(),3.0);
	}

	@Test
	void testToString() {
		Geometria test3= new Geometria(2);
		assertEquals(test3.toString(),"Geometria [id=2, nom=Circulo, area=0.0]");
	}

}
